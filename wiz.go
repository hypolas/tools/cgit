package main

import (
	"github.com/pterm/pterm"
	"os"
	"sync"

	"github.com/charmbracelet/lipgloss"
	tea "gitlab.com/hypolas/tools/bubbletea"
)

var (
	qsList []Question
	p      *tea.Program
	wg     sync.WaitGroup
)

const (
	Green  = "#66FF66"
	Violet = "#DD55AA"
	Gris   = "#AAAAAA"
)

type Styles struct {
	BorderColor lipgloss.Color
	InputField  lipgloss.Style
}

func DefaultStyles() *Styles {
	s := new(Styles)
	s.BorderColor = lipgloss.Color(Green)
	s.InputField = lipgloss.NewStyle().
		BorderForeground(s.BorderColor).
		BorderStyle(lipgloss.NormalBorder()).
		Padding(1).
		Width(63).
		Foreground(lipgloss.Color(Violet)).
		Bold(true)

	return s
}

func TitleDefaultStyles() *Styles {
	s := new(Styles)
	s.InputField = lipgloss.NewStyle().
		Padding(1).
		Width(63).
		Foreground(lipgloss.Color(Green)).
		Bold(true)

	return s
}

func BottomDefaultStyles() *Styles {
	s := new(Styles)
	s.InputField = lipgloss.NewStyle().
		PaddingLeft(2).
		Width(63).
		Foreground(lipgloss.Color(Gris))

	return s
}

func ButtonCMDStyle() *Styles {
	s := new(Styles)
	s.InputField = lipgloss.NewStyle().
		Bold(true)

	return s
}

func BottomMenuCMDStyle() *Styles {
	s := new(Styles)
	s.InputField = lipgloss.NewStyle().
		PaddingLeft(2).
		Width(63)

	return s
}

type Main struct {
	styles       *Styles
	titleStyles  *Styles
	bottomStyles *Styles
	index        int
	questions    []Question
	width        int
	height       int
	done         bool
	menuBottom   string
}

type Question struct {
	question     string
	answer       string
	key          string
	defaultValue string
	funcNew      func()
	placeholder  string
	input        Input
}

func (qs *Question) newShortQuestion() {
	model := qs.NewShortAnswerField()
	qs.input = model
}

func (qs *Question) newLongQuestion() {
	model := qs.NewLongAnswerField()
	qs.input = model
}

func New(questions []Question) *Main {
	styles := DefaultStyles()
	titlestyle := TitleDefaultStyles()
	bottomStyle := BottomDefaultStyles()
	prev := ButtonCMDStyle().InputField.Render("CTRL + <=")
	next := ButtonCMDStyle().InputField.Render("CTRL + =>")
	exit := ButtonCMDStyle().InputField.Render("CTRL + C")
	mb := "Previous: " + prev + "  |  Next: " + next + "  |  Exit: " + exit
	return &Main{styles: styles, questions: questions, titleStyles: titlestyle, bottomStyles: bottomStyle, menuBottom: mb}
}

func (m Main) Init() tea.Cmd {
	return m.questions[m.index].input.Blink
}

func (m Main) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	current := &m.questions[m.index]
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.width = msg.Width
		m.height = msg.Height
	case tea.KeyMsg:
		switch msg.Type {
		case tea.KeyCtrlC:
			return m, tea.Quit
		case tea.KeyCtrlRight:
			if m.index == len(m.questions)-1 {
				m.done = true
			}
			current.answer = current.input.Value()
			m.Next()
			return m, current.input.Blur
		case tea.KeyCtrlLeft:
			current.answer = current.input.Value()
			m.Prev()
			return m, current.input.Blur
		}
	}
	current.input, cmd = current.input.Update(msg)
	return m, cmd
}

func (m Main) View() string {
	if m.done {
		for _, q := range m.questions {
			if q.key == cmdBeforeClone {
				cfg.CmdBeforeClone = q.answer
			} else if q.key == cmdAfterClone {
				cfg.CmdAfterClone = q.answer
			} else if q.key == folderIsPresent {
				cfg.CmdFolderIsPresent = q.answer
			} else if q.key == storageLocation {
				cfg.Storage = q.answer
			}
		}

		wg.Add(1)
		go func() {
			wg.Done()
			p.Quit()
		}()
		wg.Wait()

		return ""
	}

	current := m.questions[m.index]
	return lipgloss.Place(
		m.width,
		m.height,
		lipgloss.Center,
		lipgloss.Center,
		lipgloss.JoinVertical(
			lipgloss.Left,
			m.titleStyles.InputField.Render(current.question),
			m.styles.InputField.Render(current.input.View()),
			BottomMenuCMDStyle().InputField.Render(m.menuBottom),
		),
	)
}

func (m *Main) Next() {
	if m.index < len(m.questions)-1 {
		m.index++
	} else {
		m.index = 0
	}
}

func (m *Main) Prev() {
	if m.index > 0 {
		m.index--
	} else {
		m.index = 0
	}
}

func wiz() {
	// init styles; optional, just showing as a way to organize styles
	// start bubble tea and init first model
	questionList()
	main := New(qsList)
	var err error
	if debug {
		logTeaFile, err = tea.LogToFile(fullDebugTeaPath, "debug")
		if err != nil {
			pterm.Println("fatal:", err)
			os.Exit(1)
		}
		defer logTeaFile.Close()
	}

	p = tea.NewProgram(*main, tea.WithAltScreen(), tea.WithContext(teaContext))
	if _, err := p.Run(); err != nil {
		log.Error(err)
	}
}

func questionList() {
	gitpath := ""
	if cfg.Storage != "" {
		gitpath = cfg.Storage
	} else if dir, err := os.UserHomeDir(); err == nil {
		gitpath = dir
	}

	qs0 := Question{
		question:     "What is the root directory of your git repositories?",
		key:          storageLocation,
		defaultValue: gitpath,
	}
	qs0.funcNew = qs0.newShortQuestion
	qs0.funcNew()

	qs1 := Question{
		question: `This script will be started before clone.
Interpreter: ` + interpreter + `.
{{ .PPath }} == project path location on disk.`,
		key:          cmdBeforeClone,
		placeholder:  "echo \"Exemple: Clone into {{ .PPath }}\"",
		defaultValue: cfg.CmdBeforeClone,
	}
	qs1.funcNew = qs1.newLongQuestion
	qs1.funcNew()

	qs2 := Question{
		question: `This script will be started if the target directory exists.
Interpreter: ` + interpreter + `.
{{ .PPath }} == project path location on disk.`,
		key:          folderIsPresent,
		placeholder:  "cd {{ .PPath }} && git pull && code .",
		defaultValue: cfg.CmdFolderIsPresent,
	}
	qs2.funcNew = qs2.newLongQuestion
	qs2.funcNew()

	qs3 := Question{
		question: `This script will be started at the end.
Interpreter: ` + interpreter + `.
{{ .PPath }} == project path location on disk.`,
		key:          cmdAfterClone,
		placeholder:  "cd {{ .PPath }} && code .",
		defaultValue: cfg.CmdAfterClone,
	}
	qs3.funcNew = qs3.newLongQuestion
	qs3.funcNew()

	qsList = append(qsList, qs0, qs1, qs2, qs3)
}
