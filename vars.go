package main

import (
	"context"
	"github.com/gookit/slog"
	"os"
)

const (
	configFile   = "config.yaml"
	debugTeaFile = "debugTea.log"
	debugSysFile = "debugSys.log"
)

var (
	log               *slog.Logger = slog.NewWithHandlers(slog.NewStd())
	reconfigure       *bool
	disableAllScripts bool
	configPath        string
	fullConfigPath    string
	fullDebugTeaPath  string
	fullDebugSysPath  string
	AppVersion        string
	cfg               CFG
	urlRepo           string
	logTeaFile        *os.File
	logSysFile        *os.File
	teaContext        context.Context = context.Background()
	progTerminal      []string
	folderExist       bool
	scriptExtension   string
	interpreter       string
	debug             bool
)
