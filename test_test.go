package main

import (
	"github.com/pterm/pterm"
	"testing"
)

func TestURL(t *testing.T) {
	testURLS := []string{
		"https://gitlab.com/hypolas/tools/cgit.git",
		"git@gitlab.com:hypolas/tools/cgit.git",
		"git@github.com:hypolas/cgit.git",
	}

	for _, urlR := range testURLS {
		parseURL(urlR)
		log.Println(uRLToFolder)
	}
}

func TestWiz(t *testing.T) {
	wiz()
}

func TestFindTerm(t *testing.T) {
	findTerminal()
	pterm.Println(progTerminal)
}
