package main

import (
	"bufio"
	"bytes"
	"flag"
	"github.com/pterm/pterm"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"text/template"
	"time"

	_ "embed"

	"github.com/gookit/slog"
	"github.com/gookit/slog/handler"
	"gopkg.in/yaml.v3"
)

var (
	uRLToFolder URLToFolder
)

func init() {
	if runtime.GOOS == "windows" {
		configPath = os.Getenv("AppData") + string(os.PathSeparator) + "cgit_hypolas" + string(os.PathSeparator)
	} else {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			panic(err)
		}

		configPath = homeDir + string(os.PathSeparator) + ".config/cgit_hypolas/"
	}

	if os.Getenv("CGIT_CI") == "OS_TEST" {
		configPath = "/tmp/"
	}

	fullConfigPath = configPath + configFile
	fullDebugTeaPath = configPath + debugTeaFile
	fullDebugSysPath = configPath + debugSysFile

	if _, err := os.Stat(fullConfigPath); err != nil {
		os.MkdirAll(configPath, os.ModePerm)
	}

	// var err error
	// logSysFile, err = os.OpenFile(fullDebugSysPath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	// if err != nil {
	// 	log.Fatalf("error opening file: %v", err)
	// }

	h1 := handler.MustFileHandler(fullDebugSysPath, handler.WithLogLevels(slog.DangerLevels))
	log.PushHandler(h1)
	findTerminal()
}

func main() {
	defer log.MustClose()
	// Close system log file at end
	// defer logSysFile.Close()

	// Option de lancement
	flag.StringVar(&urlRepo, "link", "", "Link to the git repository")
	flag.StringVar(&urlRepo, "l", "", "Link to the git repository (short)")
	version := flag.Bool("version", false, "Print version")
	flag.BoolVar(&debug, "debug", false, "Enable debug. Written next to the configuration file")
	infos := flag.Bool("info", false, "Print information about application")
	jinfos := flag.Bool("jinfo", false, "Print info to json for test")
	reconfigure = flag.Bool("reconfigure", false, "Reconfigure your application configs")
	flag.BoolVar(&disableAllScripts, "disablescripts", false, "Disable all scripts")
	flag.BoolVar(&disableAllScripts, "ds", false, "Disable all scripts (short)")
	flag.Parse()

	// Print version
	if *version {
		pterm.Println("version: ", AppVersion)
		os.Exit(0)
	}

	// Print infos
	if *infos {
		giveInfos("")
	}

	// Print infos au format json
	if *jinfos {
		giveInfos("json")
	}

	// Créé la configuration et/ou la charge
	createOrLoadConfig()

	// Check some variable before
	check()

	parseURL(urlRepo)

	uRLToFolder.createFolder()

	if cfg.CmdBeforeClone != "" && !disableAllScripts {
		runCMD(cfg.CmdBeforeClone)
	}

	if cfg.CmdFolderIsPresent != "" && folderExist && !disableAllScripts {
		runCMD(cfg.CmdFolderIsPresent)
	}

	if !folderExist {
		os.Chdir(uRLToFolder.RootFolderPath)
		cmd := exec.Command("git", "clone", urlRepo)
		cmd.Env = os.Environ()
		stdout, _ := cmd.StdoutPipe()
		stderr, _ := cmd.StderrPipe()

		err := cmd.Start()
		checkErr(err)

		// print the output of the subprocess
		go func() {
			scanner := bufio.NewScanner(stdout)
			for scanner.Scan() {
				m := scanner.Text()
				pterm.Println(m)
			}
		}()

		go func() {
			scanner := bufio.NewScanner(stderr)
			for scanner.Scan() {
				m := scanner.Text()
				pterm.Println(m)
			}
		}()

		cmd.Wait()
	}

	if cfg.CmdAfterClone != "" && !disableAllScripts {
		runCMD(cfg.CmdAfterClone)
	}
}

// createFolder create parent folder of project
func (uTF *URLToFolder) createFolder() {
	pathSplitted := strings.Split(uTF.Path, "/")
	folder := filepath.Join(pathSplitted[0 : len(pathSplitted)-1]...)
	uTF.RootFolderPath = cfg.Storage + string(os.PathSeparator) + uTF.Domain + string(os.PathSeparator) + folder
	uTF.Folder = strings.Split(pathSplitted[len(pathSplitted)-1], ".")[0]
	cfg.PPath = uTF.RootFolderPath + string(os.PathSeparator) + uTF.Folder

	if _, err := os.Stat(cfg.PPath); err == nil {
		pterm.Println("Folder is present: true")
		folderExist = true
		return
	} else {
		pterm.Println("Folder is present: false")
	}

	os.MkdirAll(uTF.RootFolderPath, os.ModePerm)
	pterm.Println("PPath:", cfg.PPath)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

// check: check important setup
func check() {
	if urlRepo == "" {
		pterm.Println("No links provided. Usage: cgit -h")
		os.Exit(1)
	}

	if !strings.HasSuffix(urlRepo, ".git") {
		pterm.Println("l'URL not finish with \".git\"")
		os.Exit(1)
	}

	if cfg.Storage == "" {
		pterm.Println("Storage not configured. Please run: \"cgit -reconfigure\"")
		os.Exit(1)
	}
}

// createOrLoadConfig load or create config file
func createOrLoadConfig() {
	if _, err := os.Stat(fullConfigPath); err != nil || *reconfigure {
		area, err := pterm.DefaultArea.WithFullscreen().WithCenter().Start()
		if err != nil {
			log.Error(err)
		}

		if err != nil {
			log.Error(err)
		}

		switchTitle := cGitTile()
		for _, title := range switchTitle {
			area.Update(title)
			time.Sleep(420 * time.Millisecond)
		}
		area.Update(switchTitle[0])
		time.Sleep(600 * time.Millisecond)
		area.Clear()
		area.Stop()

		cfgBytes, _ := os.ReadFile(fullConfigPath)
		yaml.Unmarshal(cfgBytes, &cfg)
		wiz()
		cfg.Storage = strings.TrimRight(cfg.Storage, string(os.PathSeparator))

		configBytes, err := yaml.Marshal(cfg)
		if err != nil {
			panic(err)
		}

		err = os.MkdirAll(configPath, os.ModePerm)
		if err != nil {
			panic(err)
		}

		err = os.WriteFile(fullConfigPath, configBytes, os.ModePerm)
		if err != nil {
			panic(err)
		}

		pterm.DefaultBox.Printf("Configs file will be stored in: \n -> %s\n\ncgit is now configured.\ncgit -reconfigure if you want reconfigure cgit.", fullConfigPath)
		print("\n")
		if urlRepo == "" {
			os.Exit(0)
		}
	}

	cfgBytes, err := os.ReadFile(fullConfigPath)
	if err != nil {
		panic(err)
	}
	yaml.Unmarshal(cfgBytes, &cfg)

}

// parseURL: Split URL for disk structure
func parseURL(gitUrl string) {
	if strings.HasPrefix(strings.ToLower(gitUrl), "http") {
		urlParse, err := url.Parse(gitUrl)
		if err != nil {
			log.Fatalln(err)
		}
		uRLToFolder.Domain = urlParse.Host
		uRLToFolder.Path = strings.Trim(urlParse.Path, "/")
	} else {
		splitUrl := strings.Split(gitUrl, ":")
		splitDomain := strings.Split(splitUrl[0], "@")
		domain := splitDomain[len(splitDomain)-1]
		if domain == "" {
			log.Fatalln("domain detection error")
		}
		uRLToFolder.Domain = domain
		uRLToFolder.Path = splitUrl[1]
	}
}

// findTerminal determine if cmd or bash
func findTerminal() {
	if runtime.GOOS == "windows" {
		path, err := exec.LookPath("cmd")
		if err == nil {
			progTerminal = []string{path, "/k"}
			interpreter = path
			scriptExtension = ".bat"
			return
		}
	} else {
		path, err := exec.LookPath("bash")
		if err == nil {
			progTerminal = []string{path, "-c"}
			interpreter = path
			scriptExtension = ".sh"
			return
		}
	}

	log.Fatalln("No termninal fond (bash or cmd)")
}

// runCMD create scripts and run it
func runCMD(script string) {
	buff := bytes.Buffer{}
	tmpl, err := template.New("script").Parse(script)
	if err != nil {
		panic(err)
	}

	err = tmpl.Execute(&buff, cfg)
	if err != nil {
		panic(err)
	}

	f, err := os.CreateTemp("", "cgit_hypolas_*"+scriptExtension)
	checkErr(err)

	f.Write(buff.Bytes())
	err = os.Chmod(f.Name(), 0700)
	checkErr(err)

	f.Close()

	defer os.Remove(f.Name())

	finalCMD := progTerminal
	finalCMD = append(finalCMD, f.Name())
	cmd := exec.Command(finalCMD[0], finalCMD[1:]...)
	cmd.Env = os.Environ()
	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()

	err = cmd.Start()
	checkErr(err)

	// print the output of the subprocess
	go func() {
		scanner := bufio.NewScanner(stdout)
		for scanner.Scan() {
			m := scanner.Text()
			pterm.Println(m)
		}
	}()

	go func() {
		scanner := bufio.NewScanner(stderr)
		for scanner.Scan() {
			m := scanner.Text()
			pterm.Println(m)
		}
	}()

	err = cmd.Wait()
	if err != nil {
		pterm.Println(err.Error())
	}
}
