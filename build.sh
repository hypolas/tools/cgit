# rsrc.exe -ico .\icon\git_icon.ico -o rsrc_windows_amd64.syso

export APP_VERSION=$(git describe --tags)
GOOS="linux" go build -ldflags="-w -s -X main.AppVersion=$APP_VERSION" -o bin/cgit
GOOS="windows" go build -ldflags="-w -s -X main.AppVersion=$APP_VERSION" -o bin/cgit.exe
upx -9 --lzma bin/*