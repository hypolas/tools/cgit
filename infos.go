package main

import (
	"encoding/json"
	"os"
	"strings"

	"github.com/pterm/pterm"
)

func giveInfos(format string) {
	info := []map[string]string{}
	var jso map[string]string = make(map[string]string)

	infos := append(info, map[string]string{
		"version": AppVersion,
	},
		map[string]string{
			"maintainer": "Nicolas HYPOLITE",
		},
		map[string]string{
			"usage": "cgit -l https://gitlab.com/hypolas/tools/cgit.git",
		},
		map[string]string{
			"config file": fullConfigPath,
		},
		map[string]string{
			"debug file": fullDebugTeaPath,
		})

	var sinfo []string
	for _, values := range infos {
		for key, value := range values {
			if format == "json" {
				jso[key] = value
			} else {
				sinfo = append(sinfo, key+": "+value)
			}
		}
	}

	if format != "json" {
		pterm.DefaultBox.Print(strings.Join(sinfo, "\n"))
		print("\n")
	}

	if format == "json" {
		printJson, _ := json.MarshalIndent(jso, "", "  ")
		pterm.Print(string(printJson))
		print("\n")
	}

	os.Exit(0)
}
