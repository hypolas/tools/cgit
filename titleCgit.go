package main

import (
	"github.com/pterm/pterm"
	"github.com/pterm/pterm/putils"
)

func cGitTile() []string {
	title := []string{}
	cg0, err := pterm.DefaultBigText.WithLetters(
		putils.LettersFromStringWithStyle("C", pterm.FgMagenta.ToStyle()),
		putils.LettersFromStringWithStyle("GIT", pterm.FgGreen.ToStyle()),
	).Srender()

	cg1, err := pterm.DefaultBigText.WithLetters(
		putils.LettersFromStringWithStyle("T", pterm.FgGreen.ToStyle()),
		putils.LettersFromStringWithStyle("C", pterm.FgMagenta.ToStyle()),
		putils.LettersFromStringWithStyle("GI", pterm.FgGreen.ToStyle()),
	).Srender()

	cg2, err := pterm.DefaultBigText.WithLetters(
		putils.LettersFromStringWithStyle("IT", pterm.FgGreen.ToStyle()),
		putils.LettersFromStringWithStyle("C", pterm.FgMagenta.ToStyle()),
		putils.LettersFromStringWithStyle("G", pterm.FgGreen.ToStyle()),
	).Srender()

	cg3, err := pterm.DefaultBigText.WithLetters(
		putils.LettersFromStringWithStyle("GIT", pterm.FgGreen.ToStyle()),
		putils.LettersFromStringWithStyle("C", pterm.FgMagenta.ToStyle()),
	).Srender()

	cg4, err := pterm.DefaultBigText.WithLetters(
		putils.LettersFromStringWithStyle("C", pterm.FgMagenta.ToStyle()),
		putils.LettersFromStringWithStyle("GIT", pterm.FgGreen.ToStyle()),
	).Srender()

	if err != nil {
		log.Error(err)
	}

	title = append(title, cg0, cg1, cg2, cg3, cg4)

	return title
}
