package main

const (
	projetPath      = "projetPath"
	storageLocation = "storageLocation"
	cmdBeforeClone  = "cmdBeforeClone"
	folderIsPresent = "folderIsPresent"
	cmdAfterClone   = "cmdAfterClone"
)

type URLToFolder struct {
	Domain         string
	Path           string
	Folder         string
	RootFolderPath string
}

type CFG struct {
	PPath              string `yaml:"-"`
	Storage            string `yaml:"storage"`
	CmdBeforeClone     string `yaml:"cmd_before_clone"`
	CmdFolderIsPresent string `yaml:"cmd_folder_is_present"`
	CmdAfterClone      string `yaml:"cmd_after_clone"`
}

// ///////
// CFG //
// ///////
// type CFG struct {
// 	Storage Storage `yaml:"storage"`
// 	Options Options `yaml:"options"`
// }
// type Storage struct {
// 	GitRoot string `yaml:"git_root"`
// }
// type Options struct {
// 	RunAfter string `yaml:"run_after"`
// }

type Wizard struct {
	RootPathGit  string
	CommandAfter string
}
